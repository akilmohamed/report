<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';
header('Content-Type: application/json');
class v1 extends REST_Controller{

	public function login_get(){
		$result = array();
		$result['code'] = 100;

		
		echo json_encode($result);
	}

	public function service_list_post(){
		$this->load->model('common_model');
		$result = array();
		$result['code'] = 100;

		if(isset($_POST)){
			$coor = $_POST['location'];
			$acoor = explode(",", $coor);

		}

		$tbl = 'service';
		$awhere = array();
		$acol = array('*');
		$data = $this->common_model->get_services($awhere);
		$nData = array();

		foreach($data as $d){
			if($d['shape'] == 'polygon'){
				$apoint = explode("|",$d['range']);

				$polySides = count($apoint);
				$polyX = array();
				$polyY = array();

				foreach($apoint as $ap){
					$ac = explode(",", $ap);
					$polyX[] = $ac[0];
					$polyY[] = $ac[1];
				}

				$s = $this->common_model->pointInPolygon($polySides, $polyX, $polyY, $acoor[0], $acoor[1]);
				if($s == 1){
					$nData[] = $d;
				}
			}
			if($d['shape'] == 'circle'){
				$aparam = explode("|", $d['range']);

				$radius = $aparam[0];
				$acenter = explode(",",$aparam[1]);
				$centerPA = $acenter[0];
				$centerPB = $acenter[1];

				$s = $this->common_model->arePointsNear($acoor[0],$acoor[1],$centerPA,$centerPB,$radius);
				if($s == 1){
					$nData[] = $d;
				}
			}
		}

		if(!empty($nData[0])){
			$result['code'] = 200;
			$result['data'] = $nData;
		}else{

		}
		

		
		echo json_encode($result);
	}

	public function submit_report_post(){
		$this->load->model('common_model');
		$result = array();
		$result['code'] = 100;

		if(!empty($_POST)){
			$tbl = 'report';
			$aval = array();
			foreach($_POST as  $k=>$p){
				$aval[$k] = $p;
			}

			$nid = $this->common_model->add($tbl, $aval);
			$result['ins_id'] = $nid;
			$result['code'] = 200;
		}

		
		echo json_encode($result);
	}

	public function update_user_post(){
		$this->load->model('common_model');
		$result = array();
		$result['code'] = 100;

		if(!empty($_POST)){
			$tbl = 'users';
			$aset = array();
			foreach($_POST as  $k=>$p){
				if($k != 'userid'){
					$aset[$k]=$p;
				}
			}
			$awhere['id'] = $_POST['userid'];

			$this->common_model->update($tbl, $aset, $awhere);
			
		}

		
		echo json_encode($result);
	}

	

}