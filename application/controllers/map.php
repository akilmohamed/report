<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class map extends CI_Controller {
	public $aheader = array();
	public $afooter = array();
	
	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('map');
	}
}