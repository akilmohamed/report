<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('common_model');
	}
	
	public function index(){
		$adata = array();
		if(!empty($_POST['adminid'])){
			$this->session->set_userdata(array("adminid"=>$_POST['adminid']));
		}

		$tbl = 'sp';
		$awhere = array("id"=>$this->session->userdata('adminid'));
		$acol = array();
		$spdata =$this->common_model->detail($tbl, $awhere, $acol, NULL,  NULL );

		$tbl = 'service';
		$awhere = array("spid"=>$this->session->userdata('adminid'));
		$acol = array();
		$sdata =$this->common_model->detail($tbl, $awhere, $acol, NULL,  NULL );

		$count = 0;
		$allreport = array();
		foreach($sdata as $s){
			$tbl = 'report';
			$awhere = array("service_id"=>$s['id']);
			$acol = array();
			$areport =$this->common_model->detail($tbl, $awhere, $acol, NULL,  NULL );
			$allreport[] = $areport;
			$c = count($areport);
			$count = $count+$c;
		}

		$adata['total_report']= $count;
		$adata['all_report'] = $allreport[0];
		$adata['spdata'] = $spdata;
		$adata['sdata'] = $sdata;

		$this->load->view('admin/dashboard', $adata);
	}
}
