<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class common_model extends CI_Model {
	function __construct(){
		parent::__construct();

		$this->load->database();
	}
	function detail($tbl = NULL, $awhere = array(), $acol = array(), $sort_by = NULL, $group_by = NULL ){
		$arr_config = array();
		$arr_config['table'] 	= $tbl;
		$arr_config['col'] 		= array( '*' );
		if( !empty($acol) ){
			$arr_config['col'] = $acol;
		}
		if( !empty($awhere) ){
			$arr_config['where'] = $awhere;
		}
		if( !empty($sort_by) ){
			$arr_config['order_by'] = $sort_by;
		}
		if( !empty($group_by) ){
			$arr_config['group_by'] = $group_by;
		}
		$adata = parent::__query_select($arr_config);
		//echo '<br>[last query]'.$this->db->last_query();
		if( !empty($adata) ){
			return $adata;
		}
		return 0;
	}
	function get_services( $awhere = array()){
		$aresult = array();
		$this->db->select('*');
		$this->db->from('service s');
		$this->db->join('sp p', 'p.id = s.spid');
		
		if(!empty($awhere)){
			$this->db->where($awhere);
		}

		$query = $this->db->get();
		
		//echo $this->db->last_query();
		
		if($query->num_rows()>0) $aresult = $query->result_array();
		$query->free_result();
		
		return $aresult;	
	}
	function add($tbl = NULL, $aval = array()){
		$arr_config['table'] = $tbl;
		$arr_config['val'] 	 = $aval;
		$new_id = parent::__query_insert($arr_config);
		//echo '<br>[2]'.$this->db->last_query();
		return $new_id;
	}
	function delete($tbl = NULL, $id = NULL){
		$arr_config['table'] = $tbl;
		$arr_config['set'] = array(
			'bactive' => 0,
			'supdatedip' => $this->input->ip_address()
		);
		$arr_config['where'] = array('id'=>$id);
		$this->db->set('dupdated', 'sysdate()', FALSE);
		return parent::__query_update($arr_config);
		//echo '<br>[2]'.$this->db->last_query();
	}
	

	function update($tbl = NULL, $aset = array(), $awhere= array()){
		$arr_config['table'] = $tbl;
		$arr_config['set'] 	 = $aset;
		$arr_config['where'] = $awhere;
		$adata = parent::__query_update($arr_config);
		return $adata;
	}


	function pointInPolygon($polySides,$polyX,$polyY,$x,$y) {
		$j = $polySides-1 ;
		$oddNodes = 0;
		for ($i=0; $i<$polySides; $i++) {
			if ($polyY[$i]<$y && $polyY[$j]>=$y ||  $polyY[$j]<$y && $polyY[$i]>=$y) {
		    	if ($polyX[$i]+($y-$polyY[$i])/($polyY[$j]-$polyY[$i])*($polyX[$j]-$polyX[$i])<$x)    {
		    		$oddNodes=!$oddNodes; 
		    	}
		   	}
		   $j=$i; 
		}

		return $oddNodes; 
	}

	function arePointsNear($checkPA,$checkPB,$centerPA,$centerPB,$radius) {
		$ky = 40000 / 360;
		$kx=cos(M_PI*$centerPA/180.0)*$ky;
		$dx=abs($centerPB-$checkPB)*$kx;
		$dy=abs($centerPA-$checkPA)*$ky;
		$result=sqrt($dx * $dx + $dy * $dy)<= ($radius/1000);

		if($result)
		return 1;
		else
		return 0;
	}



}


