<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<style>
		html { 
			background: url('<?php echo base_url("public/img/bg.jpg");?>') no-repeat center center fixed; 
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}
		.parent {position: relative;height: 100%;}
		#login-case {width: 300px;
			border-radius: 4px;
		  height: 130px;
		  padding: 20px;
		  background: #fff;
		  position: absolute;
		  top: 50%;
		  left: 50%;

		  margin: -85px 0 0 -170px;}
		  input[type="text"] {border: 1px solid #999; border-radius: 4px;padding:10px;margin-top: 40px;width:226px;}
		  input[type="submit"] {border: 1px solid #999; border-radius: 4px;padding:10px;cursor: pointer;}
		  input[type="submit"]:hover {background:#999;color:#fff;}
	</style>
</head>
<body>

	<div id="login-case">
		<form action="<?php echo base_url('admin/dashboard');?>" method="post">
			<input type="text" placeholder="Admin ID" name="adminid"/>
			<input type="submit" value="Go"/>
		</form>
	</div>


</body>
</html>