<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<style>
		html { 
			font-family: 'arial';
			background: url('<?php echo base_url("public/img/bg.jpg");?>') no-repeat center center fixed; 
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}

		#container {background: #fff;padding:15px;margin:50px auto;max-width: 900px;border-radius: 4px;}
		#container h1 {font-weight: normal;text-transform: uppercase;margin-bottom: 0}
		#container h4 {font-weight: normal;color:#999;text-transform: capitalize;margin-top:0;}

		h3 a {font-size: 12px !important;}

		table {width: 100%}
		table td, th {padding: 5px;text-align: left;}

		
	</style>
</head>
<body>

	<div id="container">
		<h1><?php echo $spdata[0]['name']; ?></h1>
		<h4><?php echo $spdata[0]['service']; ?></h4>
		
		<h3 style="margin-bottom:0;font-weight:normal">Total Service Region : <?php echo count($sdata);?> <a href="<?php echo base_url('admin/map');?>">Add Region</a></h3>
		<h3 style="margin-bottom:0;font-weight:normal">Total Report : <?php echo $total_report;?></h3>
		<br>
		<hr>
		<br>
		<h2 style="margin-bottom:0;font-weight:normal">Reports list</h2>
		<?php 
			if(!empty($all_report)){
				echo '<table>
				<tr>
					<th>#</th>
					<th>User</th>
					<th>Location</th>
					<th>Desc</th>
				</tr>
				';

				foreach($all_report as $k=>$r){
					echo '
					<tr>
						<td>'.($k+1).'</td>
						<td>'.$r['userid'].'</td>
						<td>'.$r['location'].'</td>
						<td>'.$r['title'].'</td>
					</tr>
					';
				}
			}
		?>
	</div>

</body>
</html>