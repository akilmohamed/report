<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #vertices {float:left;width: 19%;}
      #map-canvas {
      	float: right;
      	 min-width: 200px;
		  width: 80%;
		  min-height: 200px;
		  height: 100%;
      }	

      .left-bar {background:#fff;width: 15%;min-height: 200px;}

      .left {float:left;height: 100%;}
      .right {width:84%;height: 100%;}

      .add {padding:4px;border: 1px solid #999;background: #e5e5e5}
    </style>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/normalize.css');?>">

	<script type="text/javascript" src="<?php echo base_url('public/js/jquery-1.11.3.min.js');?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=drawing"></script>
  

</head>
<body>
	<div id="vertices"></div>
	
	<div id="map-canvas"></div>
	


	 	<script>
   	var map;
   	var iw = new google.maps.InfoWindow(); 

   	var lat_longs = new Array();
	var markers = new Array();
	var drawingManager;
	var newShape;

	function initialize() {
		var myLatlng = new google.maps.LatLng(3.0702001, 101.6101097);
		var myOptions = {
			  		zoom: 17,
					center: myLatlng,
			  		mapTypeId: google.maps.MapTypeId.ROADMAP
			  	}
		map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
		drawingManager = new google.maps.drawing.DrawingManager({
			drawingMode: google.maps.drawing.OverlayType.POLYGON,
	  		drawingControl: true,
	  		drawingControlOptions: {
	  				position: google.maps.ControlPosition.TOP_CENTER,
					drawingModes: [
						google.maps.drawing.OverlayType.POLYGON,
						google.maps.drawing.OverlayType.CIRCLE,
					]
			},
			polygonOptions: {
				editable: true,
            	fillColor:"#F1756C",
            	strokeColor:"#F1756C",
   				draggable: true
			},
	        polylineOptions: {
	            strokeColor:"#D8483E"
	        },
	        circleOptions: {
	        	fillColor: '#F1756C',
	        	strokeColor: '#F1756C',
			    fillOpacity: 0.4,
			    strokeWeight: 3,
			    clickable: true,
			    editable: true,
   				draggable: true,
			    zIndex: 1
	        }
		});
		
		drawingManager.setMap(map);
			
		google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
			newShape = event.overlay;
			newShape.type = event.type;

			if(newShape.type == "polygon"){
	            overlayClickListener(event.overlay);
	           	// $('#vertices').html(event.overlay.getPath().getArray());
	           	var coordinates = (newShape.getPath().getArray());
				var s = '';
				coordinates.forEach(function(i,v,array){
					s += v+" : "+i+"<br>";
				})
				$('#vertices').html(s+"<div class='add'>Add</div>");
	        }

	        if(newShape.type == "circle"){
	        	var s = newShape.getRadius()+"|"+newShape.getCenter();
	        		$('#vertices').html(s+"<div class='add'>Add</div>");
	        }
			
			

		});

        google.maps.event.trigger(map, "resize");
    }



	function overlayClickListener(overlay) {
	    google.maps.event.addListener(overlay, "mouseup", function(event){
	       var coordinates = (overlay.getPath().getArray());
			var s = '';
			coordinates.forEach(function(i,v,array){
				s += v+" : "+i+"<br>";
			})
			$('#vertices').html(s+"<div class='add'>Add</div>");
	    });
	}

	initialize();

	$(function(){
	    $('#save').click(function(){
	        //iterate polygon vertices?
	    });
	});

    </script>

</body>
</html>