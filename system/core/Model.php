<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		log_message('debug', "Model Class Initialized");
	}

	/**
	 * __get
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	
	/** common function to excute database **/
	
	function __query_select( $arr_config = array() ){
		if(!empty($arr_config['table'])){
			$aresult = 0;
			
			$this->db->select( empty($arr_config['col']) ? '*' : $arr_config['col'] )->from($arr_config['table']);

			if( !empty($arr_config['where']) ) 	  $this->db->where($arr_config['where']);
			if( !empty($arr_config['limit']) ) 	  $this->db->limit($arr_config['limit']);
			if( !empty($arr_config['order_by']) ) $this->db->order_by($arr_config['order_by']);
			if( !empty($arr_config['group_by']) ) $this->db->group_by($arr_config['group_by']);
			
			$query = $this->db->get();
			
			if($query->num_rows()>0) $aresult = $query->result_array();
					
			$query->free_result();
			if(!empty($arr_config['debug'])) echo $this->db->last_query();

			return $aresult;
		}else{
			return 0;	
		}
	}	
	function __query_sql($sql = NULL){
		if(!empty($sql)){
			$query = $this->db->query($sql);
			//echo $this->db->last_query();
			if ($query->num_rows() > 0) return $query->result_array();
		}
		return false;	
	}
	function __query_insert($arr_config = array()){
		if(!empty($arr_config)){
			$this->db->insert($arr_config['table'], $arr_config['val']);
			return $this->db->insert_id();
		}
		return false;
	}
	function __query_update($arr_config = array()){
		if( !empty($arr_config['table']) && !empty($arr_config['set']) && !empty($arr_config['where']) ){
			
			$this->db->update($arr_config['table'], $arr_config['set'], $arr_config['where']);
			
			if( $this->db->affected_rows() ) return true;		
		}
		return false;
	}
	/** END **/
	
}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */