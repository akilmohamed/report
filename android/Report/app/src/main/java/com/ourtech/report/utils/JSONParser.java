package com.ourtech.report.utils;

import android.content.Context;
import android.util.Log;

import com.ourtech.report.ServiceList.ServiceList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ShockTechie on 6/13/2015.
 */
public class JSONParser {

    private String url;
    private Context context;
    private int method;

    private String jsonStr = "";
    private JSONArray jsonArray = new JSONArray();

    private ServiceHandler sh;

    public JSONParser(String url, int method, Context context){
        //CacheRequests.getInstance().initIfNeed();
        this.url = url;
        this.method = method;
        this.context = context;

        this.sh = new ServiceHandler();

    }

    public List<ServiceList> parseServiceList(){
        List <ServiceList> result = new ArrayList<ServiceList>();

        try {

            jsonStr = sh.makeServiceCall(url, method);

            JSONObject jsonObj = new JSONObject(jsonStr);
            jsonArray = jsonObj.getJSONArray("data");
           if(jsonObj.getString("code").equals("200")) {
               for (int i = 0; i < jsonArray.length(); i++) {
                   result.add(new ServiceList((JSONObject) jsonArray.get(i)));
               }
           }else{

           }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


}
