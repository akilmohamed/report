package com.ourtech.report.ServiceList;

import org.json.JSONObject;

/**
 * Created by ShockTechie on 6/13/2015.
 */
public class ServiceList {

    private Integer serviceId;
    private String name;
    private String sservice;

    public ServiceList (JSONObject object){

        if(object.has("name")){
            setName(object.optString("name"));
        }
        if(object.has("service")){
            setSservice(object.optString("service"));
        }
        if(object.has("id")){
            setServiceId(object.optInt("id"));
        }

    }

    public void setName(String s) { this.name = s;}
    public String getName() {return name;}

    public void setSservice(String s) { this.sservice = s;}
    public String getSservice() {return sservice;}

    public void setServiceId(Integer i) { this.serviceId = i;}
    public Integer getServiceId() {return serviceId;}
}
