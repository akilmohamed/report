package com.ourtech.report.utils;

/**
 * Created by ShockTechie on 6/13/2015.
 */
public class Constants {

    public static final String API_URL = "http://report.ourtech.my/api/v1/";
    public static final String API_LIST = "service_list";
    public static final String API_REPORT = "submit_report";

}
