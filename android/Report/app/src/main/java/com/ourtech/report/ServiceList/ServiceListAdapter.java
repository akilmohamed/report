package com.ourtech.report.ServiceList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ourtech.report.R;
import com.ourtech.report.utils.AlertDialogManager;

import java.util.List;

/**
 * Created by ShockTechie on 6/14/2015.
 */
public class ServiceListAdapter extends ArrayAdapter<ServiceList> {

    private int sdk = android.os.Build.VERSION.SDK_INT;
    private Context context;
    private List<ServiceList> servicelist;
    private AlertDialogManager alert = new AlertDialogManager();
    private LayoutInflater inflater;

    public ServiceListAdapter(Context context, List<ServiceList> servicelist){
        super (context, 0, servicelist);
        this.context = context;
        this.servicelist = servicelist;

        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View view = null;
        viewHolder holder;

        holder = new viewHolder();

        view = inflater.inflate(R.layout.v_service_list_row,parent,false);

        holder.sname = (TextView) view.findViewById(R.id.sname);
        holder.sservice = (TextView) view.findViewById(R.id.sservice);


        final ServiceList data = servicelist.get(position);

        String tname = data.getName().substring(0, 1).toUpperCase() +data.getName().substring(1);
        String tservice = data.getSservice().substring(0, 1).toUpperCase() +data.getSservice().substring(1);

        holder.sname.setText(tname);
        holder.sservice.setText(tservice);




        return view;
    }

    @Override
    public ServiceList getItem(int position) {
        return servicelist.get(position);
    }

    @Override
    public int getCount() {
        return servicelist.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    class viewHolder {
        TextView sname;
        TextView sservice;
    }

}
