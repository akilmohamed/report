package com.ourtech.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ourtech.report.ServiceList.ServiceList;
import com.ourtech.report.ServiceList.ServiceListAdapter;
import com.ourtech.report.utils.AlertDialogManager;
import com.ourtech.report.utils.Constants;
import com.ourtech.report.utils.JSONParser;
import com.ourtech.report.utils.ProgressDialogManager;
import com.ourtech.report.utils.ServiceHandler;
import com.ourtech.report.utils.SessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends Activity {

    public String Tag = "MainActivity";


    private FrameLayout content_frame;
    private FrameLayout content_frame2;
    private TextView notfound;
    private Button btnRefresh;

    private JSONArray jsonArray = new JSONArray();

    SessionManager session;
    HashMap<String, Integer> user;

    private List<NameValuePair> service_form;
    private List<NameValuePair> report_form;

    AlertDialogManager alert = new AlertDialogManager();
    ProgressDialogManager progress = new ProgressDialogManager();


    private ListView content_view;

    GPSTracker gps;

    double lat = 0.0;
    double lon = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        content_frame = (FrameLayout) findViewById(R.id.content_frame);
        content_frame2 = (FrameLayout) findViewById(R.id.content_frame2);
        notfound = (TextView) findViewById(R.id.notfound);
        btnRefresh = (Button) findViewById(R.id.btn_refresh);

        session = new SessionManager(getApplicationContext());

        user = session.getUserDetails();

        content_view = (ListView) findViewById(R.id.content_view);

        gps = new GPSTracker(MainActivity.this);




        if(gps.canGetLocation()){
            lat = gps.getLatitude();
            lon = gps.getLongitude();
        }

        Log.d("lat", Double.toString(lat));
        Log.d("lon", Double.toString(lon));

        listListener.start(false);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(gps.canGetLocation()){
                    lat = gps.getLatitude();
                    lon = gps.getLongitude();
                }

                Log.d("lat", Double.toString(lat));
                Log.d("lon", Double.toString(lon));

                listListener.start(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private AsyncTaskStarterWith listListener = new AsyncTaskStarterWith() {
        @Override
        public void start(Boolean p) {
            new ServiceListAsyncTask().execute(Constants.API_URL + Constants.API_LIST, p);
        }
    };

    private AsyncTaskStarterWith reportFormListener = new AsyncTaskStarterWith() {
        @Override
        public void start(Boolean p) {
            new ReportFormAsyncTask().execute();
        }
    };

    private class ServiceListAsyncTask extends AsyncTask<Object, Void, List<ServiceList>> {
        public ServiceListAsyncTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            showLoader(true);
            super.onPreExecute();

            if (service_form != null) {
                service_form.clear();
            }

            if(gps.canGetLocation()){
                //lat = gps.getLatitude();
                //lon = gps.getLongitude();
                lat = 3.069801;
                lon = 101.6105827;
            }


            service_form = new ArrayList<NameValuePair>(1);
            service_form.add(new BasicNameValuePair("location", Double.toString(lat)+","+Double.toString(lon)));


        }

        @Override
        protected List<ServiceList> doInBackground(Object... params) {
            /*JSONParser parser = new JSONParser((String)params[0], ServiceHandler.GET,  MainActivity.this);
            return parser.parseServiceList();*/

            List <ServiceList> result = new ArrayList<ServiceList>();

            HttpClient client = new DefaultHttpClient();
            String postURL = (Constants.API_URL+Constants.API_LIST);
            HttpPost post = new HttpPost(postURL);

            try {
                UrlEncodedFormEntity uefe = new UrlEncodedFormEntity(service_form);
                post.setEntity(uefe);
                // Execute the HTTP Post Request
                HttpResponse response = client.execute(post);
                // Convert the response into a String
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(EntityUtils.toString(resEntity));
                        jsonArray = jsonObj.getJSONArray("data");
                        if (jsonObj.getString("code").equals("200")) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                result.add(new ServiceList((JSONObject) jsonArray.get(i)));
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(final List<ServiceList> service) {
            showLoader(false);
            if(service.size() == 0){
                //empty
                notfound.setVisibility(View.VISIBLE);
            }else{
                notfound.setVisibility(View.GONE);
                final ServiceListAdapter adapter = new ServiceListAdapter(MainActivity.this, service);
                content_view.setAdapter(adapter);

                content_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        final ServiceList cdata = (ServiceList) content_view.getItemAtPosition(position);

                        View tmpV = View.inflate(getApplicationContext(), R.layout.v_report, null);

                        content_frame2.addView(tmpV);

                        final TextView report_back = (TextView) content_frame2.findViewById(R.id.back);
                        Button submit = (Button) content_frame2.findViewById(R.id.submit);
                        final EditText iptTitle = (EditText) content_frame2.findViewById(R.id.iptTitle);

                        content_frame.setVisibility(View.GONE);
                        content_frame2.setVisibility(View.VISIBLE);

                        report_back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideKeyboard();

                                content_frame.setVisibility(View.VISIBLE);
                                content_frame2.setVisibility(View.GONE);
                                content_frame2.removeAllViewsInLayout();


                            }
                        });

                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String userid, location, title;

                                if(gps.canGetLocation()){
                                    lat = gps.getLatitude();
                                    lon = gps.getLongitude();
                                }

                                Log.d("lat", Double.toString(lat));
                                Log.d("lon", Double.toString(lon));

                                userid = "1";
                                title = iptTitle.getText().toString();
                                location = Double.toString(lat) + "," + Double.toString(lon);

                                if (userid.length() <= 0 || title.length() <= 0 || location.length() <= 0) {
                                    //empty
                                    alert.showAlertDialog(MainActivity.this, "Submitting Fail.", "Please fill all the fields before submit.", false);
                                } else {
                                    if (report_form != null) {
                                        report_form.clear();
                                    }
                                    report_form = new ArrayList<NameValuePair>(4);
                                    report_form.add(new BasicNameValuePair("userid", userid));
                                    report_form.add(new BasicNameValuePair("location", location));
                                    report_form.add(new BasicNameValuePair("title", title));
                                    report_form.add(new BasicNameValuePair("service_id", Integer.toString(cdata.getServiceId())));

                                    reportFormListener.start(false);

                                    iptTitle.setText("");
                                }
                            }
                        });
                    }
                });
            }
        }

    }

    private class ReportFormAsyncTask extends AsyncTask<Void, Void, Void> {
        public ReportFormAsyncTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            //showLoader(true);
            progress.showProgressDialog(MainActivity.this, "Submitting.", "Please Wait...");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient client = new DefaultHttpClient();
            String postURL = (Constants.API_URL+Constants.API_REPORT);
            HttpPost post = new HttpPost(postURL);

            try {
                UrlEncodedFormEntity uefe = new UrlEncodedFormEntity(report_form);
                post.setEntity(uefe);
                // Execute the HTTP Post Request
                HttpResponse response = client.execute(post);
                // Convert the response into a String
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    //Log.d("RESPONSE", EntityUtils.toString(resEntity));


                }
            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            progress.hideProgressDialog();
            alert.showAlertDialog(MainActivity.this, "Thank You.", "Thank you for the report. We are here to help!", false);

            hideKeyboard();

            content_frame.setVisibility(View.VISIBLE);
            content_frame2.setVisibility(View.GONE);
            content_frame2.removeAllViewsInLayout();




        }

    }

    public interface AsyncTaskStarterWith {
        public void start(Boolean p);
    }


    private void showLoader(boolean isShow){
        if(isShow){
            progress.showProgressDialog(MainActivity.this, "Loading.", "Please Wait...");
        }else{
            progress.hideProgressDialog();
        }


    }

    public void hideKeyboard(){
        InputMethodManager imm2 = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        //Log.d(TAG, imm2.toString());
        imm2.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

}
