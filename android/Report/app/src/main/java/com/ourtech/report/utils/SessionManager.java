package com.ourtech.report.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by ShockTechie on 6/13/2015.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "FitnessManager";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String USER_ID = "UserId";
    public static final String USER_LEVEL = "UserLevel";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

        editor = pref.edit();
        editor.apply();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(int id){
        // Storing login value as TRUE
        if(id == 0) {
            editor.putBoolean(IS_LOGIN, false);

            // Storing name in pref
            editor.putInt(USER_ID, 0);
            editor.putInt(USER_LEVEL, 0);
        }else{
            editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putInt(USER_ID, id);
            editor.putInt(USER_LEVEL, 1);
        }

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            /*// user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);*/
            return false;
        }
        return true;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, Integer> getUserDetails(){
        HashMap<String, Integer> user = new HashMap<String, Integer>();
        // user level
        user.put(USER_ID, pref.getInt(USER_ID, 0));
        user.put(USER_LEVEL, pref.getInt(USER_LEVEL, 0));
        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){

    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}