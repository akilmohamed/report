package com.ourtech.report.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by ShockTechie on 6/13/2015.
 */
public class ProgressDialogManager {
    public ProgressDialog ringProgressDialog;

    public void showProgressDialog(Context context, String title, String content) {
        ringProgressDialog = ProgressDialog.show(context, title, content, true);
        ringProgressDialog.setCancelable(false);
        ringProgressDialog.setCanceledOnTouchOutside(false);


        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // Here you should write your time consuming task...
                    // Let the progress ring for 10 seconds...
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ringProgressDialog.dismiss();
            }
        }).start();*/
    }

    public void hideProgressDialog(){
        ringProgressDialog.dismiss();
    }
}
