package com.ourtech.report;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

/**
 * Created by ShockTechie on 6/13/2015.
 */
public class SplashScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Main view
        setContentView(R.layout.splash_screen);
        Thread logoTimer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                    Intent loginIntent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(loginIntent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally{
                    finish();
                }
            }
        };

        logoTimer.start();


    }
}
